<?php

$context = array(
    array("Arpatar Co. is the supplier of waterproof insulators which are in Conformity with international Standards and are Presentable throughout the world. They are used on roofs and building foundations, Toilet and bathroom sets, water channels, pools, bridges, tunnels as well as industrial buildings. The product's special features are – easy to use, long life, lightweight, tensile, easy storage .and we export the quality products based on customers’ requirements.",
        "Arpatar Co has a splendid background for many years in the selling and installation of waterproof insulators in Iran, we are pleased to help and Cooperate whit the construction and pavement companies as a consultant.",
        "Special properties",
        "Excellent overlapping and union seamless surface. Resistance against static punch and strikes. More
                    economic product.",
        "Installation",
        "And to be heated waterproof membrane should be installed by means of a torch using liquid gas in such a way that the lower surface of the membrane stick to the overlapping parts. the lower surface should be heated up to the limit that melts only the surface of the membrane.",
        "Waterproof insulators specifications:

                    Bed: meshed needles polyester + threaded tissue

                    Saturation compound: straight bitumen + app polymer",
        "easy storage",
        "Uses",
        "easy install",
        "Our work samples",
        "Installing quality insulation is effective when the proper substructure is done.
                    Therefore, all
                    surfaces should be checked before installing the insulation and its problems and defects should be
                    eliminated.",

        "Exterior walls of basements are exposed to extreme moisture and humidity due to high
                    contact with
                    moisture, especially during the rainy season. Installing and using a quality insulation can solve
                    this problem.",
        "Ready-made and bitumen-coated rolls are very efficient in preventing water leakage from
                    pools and water storage sources, which is one of the architectural concerns of the building, and are
                    also very cost-effective and cost-effective.",
        "Contact info",
        "Mobile : +98 912 3607836 ",
        "Phone : +98 21 6516 4300 ",
        "Fax : +98 21 6519 7896","layers","pool installations"),
    array("شركة Arpatar هي المورد للعوازل المقاومة للماء والتي تتوافق مع المعايير الدولية ويمكن عرضها في جميع أنحاء العالم. يتم استخدامها على الأسطح وأساسات المباني والمراحيض وأطقم الحمامات وقنوات المياه والمسابح والجسور والأنفاق وكذلك المباني الصناعية. الميزات الخاصة للمنتج هي - سهل الاستخدام ، وعمر طويل ، وخفيف الوزن ، وقابل للشد ، وسهل التخزين. ونحن نصدر منتجات عالية الجودة بناءً على متطلبات العملاء.",
        "تتمتع Arpatar Co بخلفية رائعة لسنوات عديدة في بيع وتركيب العوازل المقاومة للماء في إيران ، ويسعدنا أن نساعد ونتعاون مع شركات البناء والأرصفة كمستشار.",
        "خصائص خاصة",
        "تداخل ممتاز وسطح غير ملحوم متحد. المقاومة ضد اللكمات والضربات الثابتة. أكثر
                     منتج اقتصادي.",
        "تثبيت",
        "ولكي يتم تسخينه ، يجب تثبيت غشاء مقاوم للماء عن طريق شعلة باستخدام الغاز السائل بحيث يلتصق السطح السفلي للغشاء بالأجزاء المتداخلة. يجب تسخين السطح السفلي إلى الحد الذي يذوب فقط سطح الغشاء."

    ,"مواصفات العوازل المقاومة للماء: السرير: إبر شبكية بوليستر + نسيج ملولب مركب التشبع: بيتومين مستقيم + بوليمر تطبيق"
    ,"سهولة التخزين","الاستخدامات","تثبيت سهل","عينات عملنا",
        "يكون تركيب العزل عالي الجودة فعالاً عند الانتهاء من البنية التحتية المناسبة. لذلك يجب فحص جميع الأسطح قبل تركيب العازل وإزالة مشاكله وعيوبه.",
        "تتعرض الجدران الخارجية للطوابق السفلية للرطوبة الشديدة والرطوبة بسبب التلامس الشديد مع الرطوبة ، خاصة خلال موسم الأمطار. يمكن أن يؤدي تركيب واستخدام عزل عالي الجودة إلى حل هذه المشكلة.",
        "تعتبر الأسطوانات الجاهزة والمطلية بالبيتومين فعالة للغاية في منع تسرب المياه من أحواض السباحة ومصادر تخزين المياه ، وهو أحد الاهتمامات المعمارية للمبنى ، كما أنها فعالة للغاية من حيث التكلفة وفعالة من حيث التكلفة.",
        "معلومات الاتصال","التليفون المحمول : 989123608826+","هاتف: 982165164300+","فاكس : 982165197896+","قطاعة","تركيب حمام السباحة"
    )

);


