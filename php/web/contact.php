<?php
session_start();

?>

<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../../css/main.css">
    <script src="../../js/main.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact us</title>
</head>
<body>

<?php
    require "../util/navbar.php";
?>

</body>
</html>
