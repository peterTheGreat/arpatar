<?php
session_start();

$dir = "ltr";

if (isset($_GET["lang"])) {
    if ($_GET["lang"] === "Turkish") {
        $lang_index = 2;
        $lang = "tr";
    } else if ($_GET["lang"] === "Arabic") {
        $lang_index = 1;
        $lang = "ar";
        $dir = "rtl";
    } else {
        $lang = "en";
        $lang_index = 0;
    }
} else {
    $lang = "en";
    $lang_index = 0;
}

require "php/util/text.php";

?>


<!doctype html>
<html lang="<?php echo $lang ?>" dir="<?php echo $dir ?>">
<head>
    <link href="css/main.css" rel="stylesheet">

    <?php
    if ($lang_index == 1) {
        echo "<link href=\"css/arFont.css\" rel=\"stylesheet\">";
    } else {
        echo "<link href=\"css/enFont.css\" rel=\"stylesheet\">";
    }
    ?>
    <script src="js/main.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Arpatar</title>
</head>
<body>

<?php
require "php/util/navbar.php";
?>

<div class="banner">
    <?php
    $files = scandir("images/banner/");
    for ($i = 2; $i < count($files); $i++) {
        echo " <div class=\"swipe slider\">";
        echo " <img src=\"images/banner/" . $files[$i] . "\" alt=\"banner\"> </div>";
    }
    ?>
    <h1 class="image-text">arpatar waterproofing</h1>
</div>

<div class="layout-holder">
    <div class="main-content">
        <div class="flex">
            <div class="card">
                <h3 class="text-center"><?php
                    echo $context[$lang_index][8];
                    ?></h3>
                <img src="images/icons/apps.png" class=" round" alt="roof">
                <h3 class="text-center">
                    <?php
                    echo $context[$lang_index][18];
                    ?>
                </h3>
                <img src="images/layers.jpg" class="small-img" alt="waterproofing layers">
            </div>

            <div class="panel">
                <p class="large-text">
                    <?php
                    echo $context[$lang_index][0];
                    ?>
                </p>
                <p class="large-text">
                    <?php
                    echo $context[$lang_index][1];
                    ?>
                </p>
                <h4>
                    <?php
                    echo $context[$lang_index][2];
                    ?>
                </h4>
                <p class="large-text">
                    <?php
                    echo $context[$lang_index][3];
                    ?>
                </p>
                <h4>
                    <?php
                    echo $context[$lang_index][4];
                    ?>
                </h4>
                <p class="large-text">
                    <?php
                    echo $context[$lang_index][5];
                    ?>
                </p>
                <p class="large-text">
                    <?php
                    echo $context[$lang_index][6];
                    ?>
                    <!--                    <a class="link-to large-text" href="index.php">more info</a>-->
                </p>

            </div>
            <div class="card">
                <h3 class="text-center"> <?php
                    echo $context[$lang_index][7];
                    ?></h3>
                <img src="images/storage.jpg" alt="waterproofing" class="small-img dis-block center-margin">
                <div class="card">
                    <h3 class="text-center">
                        <?php
                        echo $context[$lang_index][9];
                        ?>
                    </h3>
                    <img src="images/cf3.jpg" alt="waterproofing" class="small-img dis-block center-margin">
                </div>

            </div>

        </div><!-- flex -->

        <div class="card">
            <h3><?php
                echo $context[$lang_index][10];
                ?></h3>
            <div class="flex section">
                <video class="center-margin video-full" controls>
                    <source src="images/arpatar_group.mp4">
                </video>
            </div>
            <div class="flex section">
                <p class="p-dec">
                    <?php
                    echo $context[$lang_index][11];
                    ?>
                </p>
                <div class="slideshow-container">
                    <div class="mySlides1 ">
                        <img src="images/af1.jpg" alt="work sample">
                    </div>
                    <div class="mySlides1 ">
                        <img src="images/af2.jpg" alt="work sample">
                    </div>
                    <div class="mySlides1 ">
                        <img src="images/af3.jpg" alt="work sample">
                    </div>
                    <div class="mySlides1 ">
                        <img src="images/af4.jpg" alt="work sample">
                    </div>
                    <div class="mySlides1 ">
                        <img src="images/af5.jpg" alt="work sample">
                    </div>
                    <div class="mySlides1 ">
                        <img src="images/af6.jpg" alt="work sample">
                    </div>

                    <a class="next" onclick="plusSlides(1, 0)">&#10095;</a>
                </div>
            </div>
            <div class="flex section">
                <p class="p-dec"><?php
                    echo $context[$lang_index][12];
                    ?></p>
                <div class="slideshow-container">
                    <div class="mySlides2">
                        <img src="images/wall1.jpg">
                    </div>

                    <div class="mySlides2">
                        <img src="images/wall2.jpg">
                    </div>
                    <div class="mySlides2">
                        <img src="images/pool3.jpg">
                    </div>


                    <a class="next" onclick="plusSlides(1, 1)">&#10095;</a>
                </div>

            </div>

            <div class="flex section">
                <p class="p-dec"><?php
                    echo $context[$lang_index][13];
                    ?></p>
                <div class="slideshow-container">
                    <div class="mySlides3">
                        <img src="images/pool1.jpg">
                    </div>
                    <div class="mySlides3">
                        <img src="images/pool2.jfif">
                    </div>

                    <a class="next" onclick="plusSlides(1, 2)">&#10095;</a>

                </div>
            </div>
            <div class="flex section">
                <p class="p-dec">
                    <?php
                    echo $context[$lang_index][19];
                    ?>
                </p>

                <div class="slideshow-container">
                    <div class="mySlides4">
                        <img src="images/pool.jpg">
                    </div>
                    <div class="mySlides4">
                        <img src="images/pool_1.jpg">
                    </div>
                    <div class="mySlides4">
                        <img src="images/pool_2.jpg">
                    </div>
                    <div class="mySlides4">
                        <img src="images/pool_final.jpg">
                    </div>
                    <div class="mySlides4">
                        <img src="images/pool4.jpg">

                    </div>

                    <a class="next" onclick="plusSlides(1, 3)">&#10095;</a>
                </div>
            </div>
        </div>
    </div><!-- main content -->
</div><!--    layout holder -->

<footer>
    <div class="info-footer flex">
        <h4><?php
            echo $context[$lang_index][14];
            ?></h4>
        <div class="footer-box">
            <span class="large-text"> <?php
                echo $context[$lang_index][15];
                ?></span>
        </div>

        <div class="footer-box">
            <span class="large-text"> <?php
                echo $context[$lang_index][16];
                ?></span>
        </div>

        <div class="footer-box">
            <span class="large-text"> <?php
                echo $context[$lang_index][17];
                ?> </span>
        </div>
        <div class="footer-box">
            <a href="https://www.instagram.com/arpatar5/" target="_blank"><img src="images/icons/ig.png" alt="instagram"
                                                                               class="icon"> : Arpatar </a></div>
    </div>
</footer>

<script>
    showSlide();
    showSlides(1, 0);
    showSlides(1, 1);
    showSlides(1, 2);
    showSlides(1, 3);
</script>
</body>
</html>
