function openSite(url){
    window.open(url,"_self");
}

let startIndex = 0;


function showSlide(){
    let i ;
    let slides = document.getElementsByClassName('slider');
    for(i = 0; i<slides.length;i++){
        slides[i].style.display = "none";
    }
    startIndex++;
    if(startIndex > slides.length){
        startIndex = 1;
    }
    slides[startIndex-1].style.display="block";
    setTimeout(showSlide,3000);
}


let slideIndex = [1,1,1,1];
/* Class the members of each slideshow group with different CSS classes */
const slideId = ["mySlides1", "mySlides2", "mySlides3","mySlides4"];



function plusSlides(n, no) {
    showSlides(slideIndex[no] += n, no);
}

function showSlides(n, no) {
    let i;
    let x = document.getElementsByClassName(slideId[no]);

    if (n > x.length) {slideIndex[no] = 1}
    if (n < 1) {slideIndex[no] = x.length}
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    x[slideIndex[no]-1].style.display = "block";
}